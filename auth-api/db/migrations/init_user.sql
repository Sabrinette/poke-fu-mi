CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY,
  username TEXT NOT NULL,
  email TEXT NOT NULL,
  user_password TEXT NOT NULL,
  user_role varchar(255) NOT NULL DEFAULT "operator",
  is_connected BOOLEAN NOT NULL CHECK (is_connected IN (0,1))
)
