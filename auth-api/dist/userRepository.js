"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const better_sqlite3_1 = __importDefault(require("better-sqlite3"));
const fs_1 = __importDefault(require("fs"));
class UserRepository {
    constructor() {
        this.db = new better_sqlite3_1.default('db/users.db', { verbose: console.log });
        this.applyMigrations();
    }
    getAllUsers() {
        const statement = this.db.prepare("SELECT * FROM users");
        const rows = statement.all();
        return rows;
    }
    getUserById(id) {
        const row = this.db.prepare("SELECT * FROM users WHERE users.id = ?").get(id);
        return row;
    }
    findUserByEmail(email) {
        const row = this.db.prepare("SELECT * FROM users WHERE users.email = ?").get(email);
        return row;
    }
    getPasswordByEmail(email) {
        const row = this.db.prepare("SELECT user_password FROM users WHERE users.email = ?").get(email);
        return JSON.stringify(row);
    }
    createUser(username, email, password, role, userState) {
        const statement = this.db.prepare("INSERT INTO users (username , email , user_password ,user_role ,is_connected) VALUES (?,?,?,?,?)");
        return statement.run(username, email, password, role, userState).lastInsertRowid;
    }
    logoutUser(id) {
        const statement = this.db.prepare("UPDATE users SET is_connected = 0 WHERE users.id = ?");
        return statement.run(id);
    }
    loginUser(id) {
        const statement = this.db.prepare("UPDATE users SET is_connected = 1 WHERE users.id = ?");
        return statement.run(id);
    }
    getAccountState(id) {
        const statement = this.db.prepare("SELECT is_connected FROM users WHERE users.id = ?");
        return JSON.stringify(statement.run(id));
    }
    getRoleUser(id) {
        const row = this.db.prepare("SELECT user_role FROM users WHERE users.id = ? ").get(id);
        return JSON.stringify(row);
    }
    applyMigrations() {
        const applyMigration = (path) => {
            const migration = fs_1.default.readFileSync(path, 'utf8');
            this.db.exec(migration);
        };
        const testRow = this.db.prepare("SELECT name FROM sqlite_schema  WHERE type = 'table' AND name = 'users'").get();
        if (!testRow) {
            console.log('Applying migrations on DB users...');
            const migrations = ['db/migrations/init_user.sql'];
            migrations.forEach(applyMigration);
        }
    }
}
exports.default = UserRepository;
//# sourceMappingURL=userRepository.js.map