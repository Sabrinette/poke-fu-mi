"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const UserController = __importStar(require("./userController"));
const register = (app) => {
    app.get('/user/role/:id', (req, res) => {
        res.status(200).json(UserController.getRole(+req.params.id));
    });
    app.post('/user/signup', (req, res) => {
        const newUser = req.body;
        res.status(200).json(UserController.signup(newUser));
    });
    //A voir
    app.post('/user/login', (req, res) => {
        const user = req.body;
        res.status(200).json(UserController.login(user));
    });
    app.put('/user/logout/:id', (req, res) => {
        res.status(200).json(UserController.logout(+req.params.id));
    });
    app.get('/user', (req, res) => {
        res.status(200).json(UserController.listUsers());
    });
    app.get('/user/:userId', (req, res) => {
        if (!UserController.getUser(+req.params.userId)) {
            res.status(404).send();
        }
        res.status(200).json(UserController.getUser(+req.params.userId));
    });
};
exports.register = register;
//# sourceMappingURL=routes.js.map