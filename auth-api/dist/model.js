"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAccount = void 0;
// Class declaration
class UserAccount {
    constructor(name, userRole, email, password) {
        this.userName = name;
        this.userRole = userRole;
        this.email = email;
        this.password = password;
    }
}
exports.UserAccount = UserAccount;
//# sourceMappingURL=model.js.map