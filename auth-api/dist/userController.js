"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUser = exports.listUsers = exports.getRole = exports.signup = exports.logout = exports.login = void 0;
const userRepository_1 = __importDefault(require("./userRepository"));
//const typedUsers: User[] = users
const userRepository = new userRepository_1.default();
const login = (userReq) => {
    try {
        const email = userReq.email;
        const password = userReq.password;
        const user = userRepository.findUserByEmail(email);
        if (user) {
            const obj = JSON.parse(userRepository.getPasswordByEmail(email));
            console.log(obj);
            const isPasswordMatch = (obj.user_password == password);
            console.log("From data base = ", userRepository.getPasswordByEmail(email));
            console.log("Actual = ", password);
            if (!isPasswordMatch) {
                console.log("Invalid Password");
                throw new Error("Invalid Password");
            }
            else {
                console.log("check state");
                const stateAccountObj = JSON.parse(userRepository.getAccountState(user.id));
                const stateAccount = stateAccountObj.is_connected;
                if (stateAccount) {
                    throw new Error("Already Connected !");
                }
                else {
                    userRepository.loginUser(user.id);
                }
            }
        }
        else {
            throw new Error("User Not Found");
        }
    }
    catch (e) {
    }
};
exports.login = login;
const signup = (userReq) => {
    try {
        const username = userReq.userName;
        const email = userReq.email;
        const password = userReq.password;
        const role = userReq.userRole;
        const user = userRepository.findUserByEmail(email);
        if (user) {
            console.log("User Already Exists");
            throw new Error("User Already Exists");
        }
        else {
            try {
                const newUser = userRepository.createUser(username, email, password, role, 1);
                return newUser;
            }
            catch (e) {
                console.log("Error while register");
                throw new Error("Error while register");
            }
        }
    }
    catch (e) {
    }
};
exports.signup = signup;
const logout = (id) => {
    userRepository.logoutUser(id);
};
exports.logout = logout;
const getRole = (id) => {
    return JSON.parse(userRepository.getRoleUser(id));
};
exports.getRole = getRole;
const listUsers = () => {
    return userRepository.getAllUsers();
};
exports.listUsers = listUsers;
const getUser = (id) => {
    return userRepository.getUserById(id);
};
exports.getUser = getUser;
//# sourceMappingURL=userController.js.map