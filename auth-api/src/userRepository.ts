import Database from 'better-sqlite3'
import fs from 'fs'
import { User } from './model'
import { UserRole } from './model'


export default class UserRepository {
  db: Database.Database


  constructor() {
    this.db = new Database('db/users.db', { verbose: console.log });
    this.applyMigrations()    
  }

  getAllUsers(): User[] {
    const statement = this.db.prepare("SELECT * FROM users")
    const rows: User[] =statement.all()
    return rows
  }

  getUserById(id : number): User {
    const row = this.db.prepare("SELECT * FROM users WHERE users.id = ?").get(id)
    return row
  }

  findUserByEmail(email : string): User {
    const row = this.db.prepare("SELECT * FROM users WHERE users.email = ?").get(email)
    return row
  }

  getPasswordByEmail(email : string) : string {
    const row = this.db.prepare("SELECT user_password FROM users WHERE users.email = ?").get(email)
    return JSON.stringify(row)
  }

  createUser(username: string, email : string , password : string , role : string, userState : number) {
    const statement = 
      this.db.prepare("INSERT INTO users (username , email , user_password ,user_role ,is_connected) VALUES (?,?,?,?,?)")
    return statement.run(username,email,password,role,userState).lastInsertRowid
  }

  logoutUser(id : number) {
    const statement = 
      this.db.prepare("UPDATE users SET is_connected = 0 WHERE users.id = ?")
    return statement.run(id)
  }

  loginUser(id : number) {
    const statement = 
      this.db.prepare("UPDATE users SET is_connected = 1 WHERE users.id = ?")
    return statement.run(id)
  }

  getAccountState(id : number) {
    const statement = 
      this.db.prepare("SELECT is_connected FROM users WHERE users.id = ?").get(id)
    return JSON.stringify(statement)
  }

  
  getRoleUser(id : number) {
    const row = this.db.prepare("SELECT user_role FROM users WHERE users.id = ? ").get(id)
    return JSON.stringify(row)
  }

  applyMigrations(){
    const applyMigration = (path: string) => {
      const migration = fs.readFileSync(path, 'utf8')
      this.db.exec(migration)
    }
    
    const testRow = this.db.prepare("SELECT name FROM sqlite_schema  WHERE type = 'table' AND name = 'users'").get()

    if (!testRow){
      console.log('Applying migrations on DB users...')
      const migrations = ['db/migrations/init_user.sql'] 	 
      migrations.forEach(applyMigration)
    }
  }
}

