import UserRepository from './userRepository'
import { User } from './model'

//const typedUsers: User[] = users
const userRepository = new UserRepository()

const login = (userReq : User) => {
    try {
        const email = userReq.email
        const password = userReq.password
        const user = userRepository.findUserByEmail(email)
        if (user) {
            const obj = JSON.parse(userRepository.getPasswordByEmail(email))
            const isPasswordMatch = (obj.user_password == password)
            if (!isPasswordMatch) {
              return JSON.parse('{"message": "Invalid Password"}');
            } else {
                console.log("check state")
                const stateAccountObj = JSON.parse(userRepository.getAccountState(user.id))
                const stateAccount = stateAccountObj.is_connected
                if(stateAccount){
                    return JSON.parse('{"message": "Already connected"}');
                }else{
                    userRepository.loginUser(user.id)
                    return userRepository.getUserById(user.id);
                }
            }
        } else {
            return JSON.parse('{"message": "user not found"}');
        }
    } catch (e) {
    }
}

const signup = (userReq : User) => {

    try {
      const username = userReq.userName
      const email = userReq.email
      const password = userReq.password
      const role = userReq.userRole as string
      const user = userRepository.findUserByEmail(email)
      if (user) {
        return JSON.parse('{"message": "User Already Exists"}');
      } else {
        try {
          const newUser = userRepository.createUser(username,email,password,role,1)
          return newUser
        } catch (e) {
          return JSON.parse('{"message": "Error while register"}'); 
        }
      }
    } catch (e) {
    }
    return userRepository.getAllUsers();
}

const logout = (id:number) => {
    userRepository.logoutUser(id)
}

const getRole = (id:number) => {
    return JSON.parse(userRepository.getRoleUser(id))
}

const listUsers = () => {
  return userRepository.getAllUsers()
}

const getUser = (id: number) => {
  return userRepository.getUserById(id)
}

const getAccountState = (id: number) => {
  return JSON.parse(userRepository.getAccountState(id))
}

export {login,logout,signup,getRole,listUsers,getUser,getAccountState}
