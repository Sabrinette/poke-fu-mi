import * as express from "express"
import * as UserController from "./userController"
import { User } from './model'

export const register = ( app: express.Application ) => {
    
    app.get('/', (req, res) => res.send('Hello World auth api!'));

    app.get('/user/role/:id', (req, res) => {
        res.status(200).json(UserController.getRole(+req.params.id))
    })

    app.post('/user/signup', (req, res) => {
        const newUser : User = req.body    
        res.status(200).json(UserController.signup(newUser))
    }) 

    //A voir
    app.post('/user/login', (req, res) => {
        const user: User = req.body    
        res.status(200).json(UserController.login(user))
    }) 
    
    app.put('/user/logout/:id', (req, res) => {
        res.status(200).json(UserController.logout(+req.params.id))
    })
    
    app.get('/user', (req, res) => {
        res.status(200).json(UserController.listUsers())
    })

    app.get('/user/:userId', (req, res) => {
        if(!UserController.getUser(+req.params.userId)){
          res.status(404).send()
        }
        res.status(200).json(UserController.getUser(+req.params.userId))
    })

    app.get('/user/accountState/:userId', (req, res) => {
        if(!UserController.getAccountState(+req.params.userId)){
          res.status(404).send()
        }
        res.status(200).json(UserController.getAccountState(+req.params.userId))
    })
}
