// Interface declaration
export interface User {
    id : number;
    userName: string;
    userRole : UserRole;
    email : string; 
    password : string;
    is_connected : number
  }
  
// Class declaration
export class UserAccount {
    id : number; 
    userName: string;
    score: number;
    userRole : UserRole;
    email : String; 
    password : String;
   
    constructor(name: string,userRole : UserRole , email : String , password : String ) {
      this.userName = name;
      this.userRole = userRole;
      this.email = email;
      this.password = password;
    }
  }
  
type Admin = "admin";
type Operator = "operator"
type UserRole = Admin | Operator

export {UserRole}
    
  
  