CREATE TABLE IF NOT EXISTS players (
  player_id INTEGER PRIMARY KEY,
  name	TEXT NOT NULL,
  score   INTEGER DEFAULT 0
  -- user_role varchar(255) NOT NULL DEFAULT "operator",
  -- user_login TEXT NOT NULL,
  -- user_password TEXT NOT NULL
)

