"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const PlayerController = __importStar(require("./playerController"));
const register = (app) => {
    app.get('/', (req, res) => res.send('Hello World docker!'));
    app.get('/player', (req, res) => {
        res.status(200).json(PlayerController.listPlayers());
    });
    app.get('/player/top', (req, res) => {
        let PlayersNumber = +req.query.number;
        res.status(200).json(PlayerController.listTopPlayers(PlayersNumber));
    });
    app.post('/player', (req, res) => {
        const newPlayer = req.body;
        res.status(200).json(PlayerController.addPlayer(newPlayer));
    });
    app.get('/player/:playerId', (req, res) => {
        if (!PlayerController.getPlayer(+req.params.playerId)) {
            res.status(404).send();
        }
        res.status(200).json(PlayerController.getPlayer(+req.params.playerId));
    });
    app.delete('/player/:playerId', (req, res) => {
        const user = req.body;
        if (!PlayerController.getPlayer(+req.params.playerId)) {
            res.status(404).send();
        }
        res.status(204).json(PlayerController.deletePlayer(+req.params.playerId, user.user_id));
    });
    app.put('/player/:playerId', (req, res) => {
        const player = req.body;
        res.status(204).json(PlayerController.updatePlayer(+req.params.playerId, player));
    });
};
exports.register = register;
//# sourceMappingURL=routes.js.map