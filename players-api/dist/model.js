"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAccount = void 0;
class UserAccount {
    constructor(name, score) {
        this.name = name;
        this.score = score;
    }
}
exports.UserAccount = UserAccount;
//# sourceMappingURL=model.js.map