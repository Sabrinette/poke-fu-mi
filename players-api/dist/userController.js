"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteUser = exports.getUser = exports.addUser = exports.listUsers = void 0;
const userRepository_1 = __importDefault(require("./userRepository"));
//const typedUsers: User[] = users
const userRepository = new userRepository_1.default();
const listUsers = () => {
    return userRepository.getAllUsers();
};
exports.listUsers = listUsers;
const addUser = (newUser) => {
    userRepository.createUser(newUser.name, newUser.login, newUser.password);
    return userRepository.getAllUsers();
};
exports.addUser = addUser;
const getUser = (id) => {
    return userRepository.getUserById(id);
};
exports.getUser = getUser;
const DeleteUser = (id) => {
    userRepository.deleteUser(id);
};
exports.DeleteUser = DeleteUser;
//# sourceMappingURL=userController.js.map