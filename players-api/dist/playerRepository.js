"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const better_sqlite3_1 = __importDefault(require("better-sqlite3"));
const fs_1 = __importDefault(require("fs"));
class PlayerRepository {
    constructor() {
        this.db = new better_sqlite3_1.default('db/players.db', { verbose: console.log });
        this.applyMigrations();
    }
    getAllPlayers() {
        const statement = this.db.prepare("SELECT * FROM players");
        const rows = statement.all();
        return rows;
    }
    getPlayerById(id) {
        const row = this.db.prepare("SELECT * FROM players WHERE players.player_id = ?").get(id);
        return row;
    }
    getTopPlayers(playersNumber) {
        const statement = this.db.prepare("SELECT * FROM players ORDER BY score DESC LIMIT ?");
        const rows = statement.all(playersNumber);
        return rows;
    }
    createPlayer(name) {
        const statement = this.db.prepare("INSERT INTO players (name) VALUES (?)");
        return statement.run(name).lastInsertRowid;
    }
    deletePlayer(id) {
        const statement = this.db.prepare("DELETE FROM players where players.player_id = ?");
        return statement.run(id);
    }
    updatePlayer(id, name, score) {
        const statement = this.db.prepare("UPDATE players SET name = ? , score = ? where players.player_id = ?");
        return statement.run(name, score, id);
    }
    applyMigrations() {
        const applyMigration = (path) => {
            const migration = fs_1.default.readFileSync(path, 'utf8');
            this.db.exec(migration);
        };
        const testRow = this.db.prepare("SELECT name FROM sqlite_schema  WHERE type = 'table' AND name = 'players'").get();
        if (!testRow) {
            console.log('Applying migrations on DB players...');
            const migrations = ['db/migrations/init.sql'];
            migrations.forEach(applyMigration);
        }
    }
}
exports.default = PlayerRepository;
//# sourceMappingURL=playerRepository.js.map