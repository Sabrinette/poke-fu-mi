"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listTopPlayers = exports.updatePlayer = exports.deletePlayer = exports.getPlayer = exports.addPlayer = exports.listPlayers = void 0;
const playerRepository_1 = __importDefault(require("./playerRepository"));
//const typedUsers: User[] = users
const playerRepository = new playerRepository_1.default();
const listTopPlayers = (playersNumber) => {
    return playerRepository.getTopPlayers(playersNumber);
};
exports.listTopPlayers = listTopPlayers;
const listPlayers = () => {
    return playerRepository.getAllPlayers();
};
exports.listPlayers = listPlayers;
const addPlayer = (newPlayer) => {
    playerRepository.createPlayer(newPlayer.name);
    return playerRepository.getAllPlayers();
};
exports.addPlayer = addPlayer;
const getPlayer = (id) => {
    return playerRepository.getPlayerById(id);
};
exports.getPlayer = getPlayer;
const deletePlayer = (id, idUser) => {
    isUserAdmin(idUser).then((result) => {
        if (result)
            playerRepository.deletePlayer(id);
    });
};
exports.deletePlayer = deletePlayer;
const updatePlayer = (id, player) => {
    playerRepository.updatePlayer(id, player.name, player.score);
};
exports.updatePlayer = updatePlayer;
function isUserAdmin(id) {
    return __awaiter(this, void 0, void 0, function* () {
        let flag = true;
        const response = yield fetch("http://0.0.0.0:5001/user/role/" + id);
        const body = yield response.text();
        if (JSON.parse(body).user_role == "operator") {
            flag = false;
        }
        return flag;
    });
}
//# sourceMappingURL=playerController.js.map