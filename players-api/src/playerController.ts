import PlayerRepository from './playerRepository'
import { Player } from './model'

const playerRepository = new PlayerRepository()

const listTopPlayers = (playersNumber:number) => {
    return playerRepository.getTopPlayers(playersNumber)
}

const listPlayers = () => {
    return playerRepository.getAllPlayers()
}
 
const addPlayer = (newPlayer: Player) => {
    playerRepository.createPlayer(newPlayer.name)
    return playerRepository.getAllPlayers()
}
  
const getPlayer = (id: number) => {
    return playerRepository.getPlayerById(id)
}

const deletePlayer = (id: number , idUser : number) => {
    isUserAdmin(idUser).then((result) => {
        if(result)
            playerRepository.deletePlayer(id)
    })
}

const updatePlayer = (id: number,player: Player) => {
    playerRepository.updatePlayer(id,player.name,player.score)
    return playerRepository.getPlayerById(id)
}

const fetch = require("node-fetch");

async function isUserAdmin(id:number): Promise<boolean> {
    let flag = true
    const response = await fetch("http://auth-api:5000/user/role/" + id)
    const body = await response.text();
    if(JSON.parse(body).user_role == "operator"){
      flag = false
    }
    return flag
 }

 async function isUserConnected(id:number): Promise<boolean> {
    let flag = true
    let value
    const response = await fetch("http://auth-api:5000/user/accountState/" + id)
    const body = await response.text();
    try{
        value = JSON.parse(body).is_connected
        if(value == 0)
          flag = false //l'utilisateur est déconnecté
    } catch (err){
        flag = false // l'utilisateur n'existe pas
    }
    return flag
 }
  
export { listPlayers , addPlayer , getPlayer, deletePlayer,updatePlayer,listTopPlayers,isUserConnected}
