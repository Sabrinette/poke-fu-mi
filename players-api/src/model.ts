// Interface declaration
export interface Player {
  name: string;
  score: number;
}

export class UserAccount {
  name: string;
  score: number;
 
  constructor(name: string, score: number) {
	this.name = name;
	this.score = score;
  }
}

type UserList = Array<Player>


