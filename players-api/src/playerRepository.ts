import Database from 'better-sqlite3'
import fs from 'fs'
import { Player } from './model'

export default class PlayerRepository {
  db: Database.Database


  constructor() {
    this.db = new Database('db/players.db', { verbose: console.log });
    this.applyMigrations()    
  }

  getAllPlayers(): Player[] {
    const statement = this.db.prepare("SELECT * FROM players")
    const rows: Player[] =statement.all()
    return rows
  }

  getPlayerById(id : number): Player {
    const row = this.db.prepare("SELECT * FROM players WHERE players.player_id = ?").get(id)
    return row
  }

  getTopPlayers(playersNumber : number): Player[] {
    const statement = this.db.prepare("SELECT * FROM players ORDER BY score DESC LIMIT ?")
    const rows: Player[] =statement.all(playersNumber)
    return rows
  }

  createPlayer(name: string) {
    const statement = 
      this.db.prepare("INSERT INTO players (name) VALUES (?)")
    return statement.run(name).lastInsertRowid
  }

  deletePlayer(id : number) {
    const statement = 
      this.db.prepare("DELETE FROM players where players.player_id = ?")
    return statement.run(id)
  }

  updatePlayer(id : number , name : string , score : number) {
    const statement = 
      this.db.prepare("UPDATE players SET name = ? , score = ? where players.player_id = ?")
    return statement.run(name,score,id)
  }

  applyMigrations(){
    const applyMigration = (path: string) => {
      const migration = fs.readFileSync(path, 'utf8')
      this.db.exec(migration)
    }
    
    const testRow = this.db.prepare("SELECT name FROM sqlite_schema  WHERE type = 'table' AND name = 'players'").get()

    if (!testRow){
      console.log('Applying migrations on DB players...')
      const migrations = ['db/migrations/init.sql'] 	 
      migrations.forEach(applyMigration)
    }
  }
}

