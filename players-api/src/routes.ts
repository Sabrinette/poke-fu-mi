import * as express from "express"
import * as PlayerController from "./playerController"
import { Player} from './model'

export const register = ( app: express.Application ) => {
  app.get('/', (req, res) => res.send('Hello World docker!'));

  app.get('/player', async(req, res) => {
    let userId : number = +req.query.user_id
    await PlayerController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })  
  	res.status(200).json(PlayerController.listPlayers())
  })

  app.get('/player/top', async(req, res) => {
    let userId : number = +req.query.user_id
    await PlayerController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })  
    let PlayersNumber : number = +req.query.number
  	res.status(200).json(PlayerController.listTopPlayers(PlayersNumber))
  })
 
  app.post('/player', async(req, res) => {
    let userId : number = +req.query.user_id
    await PlayerController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })  
	  const newPlayer: Player = req.body    
	  res.status(200).json(PlayerController.addPlayer(newPlayer))
  })  

  app.get('/player/:playerId', async(req, res) => {
    let userId : number = +req.query.user_id
    await PlayerController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })  
    if(!PlayerController.getPlayer(+req.params.playerId)){
      res.status(404).send()
    }
    res.status(200).json(PlayerController.getPlayer(+req.params.playerId))
  })

  app.delete('/player/:playerId', (req, res) => {
    let userId : number = +req.query.user_id

    if(!PlayerController.getPlayer(+req.params.playerId)){
      res.status(404).send(JSON.parse('{"message": "player not found"}'))
    }
	  res.status(204).json(PlayerController.deletePlayer(+req.params.playerId,userId))
  })  

  app.put('/player/:playerId', async (req, res) => {
    const player: Player = req.body
    let userId : number = +req.query.user_id
    await PlayerController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })    
    res.status(200).json(PlayerController.updatePlayer(+req.params.playerId,player))
  })  
}
