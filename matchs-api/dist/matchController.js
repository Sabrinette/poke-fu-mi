"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateMatch = exports.deleteMatch = exports.getMatchById = exports.addMatch = exports.listMatchs = void 0;
const matchRepository_1 = __importDefault(require("./matchRepository"));
const matchRepository = new matchRepository_1.default();
const listMatchs = () => {
    return matchRepository.getAllMatchs();
};
exports.listMatchs = listMatchs;
const addMatch = (newMatch) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (newMatch.player1 != newMatch.player2) {
                userExist(newMatch.player1).then((result) => {
                    if (result)
                        userExist(newMatch.player2).then((res) => {
                            if (res)
                                matchRepository.createMatch(newMatch.player1, newMatch.player2, newMatch.pokemons_player1, newMatch.pokemons_player2);
                            resolve(matchRepository.getAllMatchs());
                        });
                });
            }
        }, 5000);
    });
};
exports.addMatch = addMatch;
const getMatchById = (id) => {
    return matchRepository.getMatchById(id);
};
exports.getMatchById = getMatchById;
const deleteMatch = (id, idUser) => {
    isUserAdmin(idUser).then((result) => {
        if (result)
            matchRepository.deleteMatch(id);
    });
};
exports.deleteMatch = deleteMatch;
const updateMatch = (id, match) => {
    matchRepository.updateMatch(id, match.player1, match.player2, match.pokemons_player1, match.pokemons_player2, match.status, match.winner);
};
exports.updateMatch = updateMatch;
const fetch = require("node-fetch");
function userExist(id) {
    return __awaiter(this, void 0, void 0, function* () {
        let flag = true;
        const response = yield fetch("http://players-api:5000/player/" + id);
        const body = yield response.text();
        if (!body) {
            flag = false;
        }
        return flag;
    });
}
function isUserAdmin(id) {
    return __awaiter(this, void 0, void 0, function* () {
        let flag = true;
        const response = yield fetch("http://players-api:5000/user/role/" + id);
        const body = yield response.text();
        if (JSON.parse(body).user_role == "operator") {
            flag = false;
        }
        return flag;
    });
}
//# sourceMappingURL=matchController.js.map