"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const better_sqlite3_1 = __importDefault(require("better-sqlite3"));
const fs_1 = __importDefault(require("fs"));
class MatchRepository {
    constructor() {
        this.db = new better_sqlite3_1.default('db/pokemon_match.db', { verbose: console.log });
        this.applyMigrations();
    }
    getAllMatchs() {
        const statement = this.db.prepare("SELECT * FROM pokemon_match");
        const rows = statement.all();
        return rows;
    }
    createMatch(player1, player2, pokemons_player1, pokemons_player2) {
        console.log("%s ", pokemons_player1[0]);
        const statement = this.db.prepare("INSERT INTO pokemon_match (player1,player2,pokemons_player1,pokemons_player2) VALUES (?,?,?,?)");
        return statement.run(player1, player2, pokemons_player1, pokemons_player2).lastInsertRowid;
    }
    getMatchById(id) {
        const row = this.db.prepare("SELECT * FROM pokemon_match WHERE pokemon_match.id = ?").get(id);
        return row;
    }
    deleteMatch(id) {
        const statement = this.db.prepare("DELETE FROM pokemon_match where pokemon_match.id = ?");
        return statement.run(id);
    }
    updateMatch(id, player1, player2, pokemons_player1, pokemons_player2, match_status, winner) {
        const statement = this.db.prepare("UPDATE pokemon_match SET player1 = ? , player2 = ? , pokemons_player1 = ?, pokemons_player2 = ?, match_status = ?, winner = ? where players.player_id = ?");
        return statement.run(player1, player2, pokemons_player1, pokemons_player2, match_status, winner, id);
    }
    applyMigrations() {
        const applyMigration = (path) => {
            const migration = fs_1.default.readFileSync(path, 'utf8');
            this.db.exec(migration);
        };
        const testRow = this.db.prepare("SELECT name FROM sqlite_schema  WHERE type = 'table' AND name = 'pokemon_match'").get();
        if (!testRow) {
            console.log('Applying migrations on DB match...');
            const migrations = ['db/migrations/init_match.sql'];
            migrations.forEach(applyMigration);
        }
    }
}
exports.default = MatchRepository;
//# sourceMappingURL=matchRepository.js.map