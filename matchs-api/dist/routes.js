"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const MatchController = __importStar(require("./matchController"));
const register = (app) => {
    app.get('/', (req, res) => res.send('Hello World docker!'));
    app.get('/match', (req, res) => {
        res.status(200).json(MatchController.listMatchs());
    });
    app.post('/match', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const newMatch = req.body;
        let result = yield MatchController.addMatch(newMatch);
        res.status(200).json(result);
    }));
    app.get('/match/:matchId', (req, res) => {
        if (!MatchController.getMatchById(+req.params.matchId)) {
            res.status(404).send();
        }
        res.status(200).json(MatchController.getMatchById(+req.params.matchId));
    });
    app.delete('/match/:matchId', (req, res) => {
        const user = req.body;
        if (!MatchController.getMatchById(+req.params.matchId)) {
            res.status(404).send();
        }
        res.status(204).json(MatchController.deleteMatch(+req.params.matchId, user.user_id));
    });
    app.put('/match/:matchId', (req, res) => {
        const match = req.body;
        res.status(204).json(MatchController.updateMatch(+req.params.matchId, match));
    });
};
exports.register = register;
//# sourceMappingURL=routes.js.map