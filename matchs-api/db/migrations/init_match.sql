  CREATE TABLE IF NOT EXISTS pokemon_match (
    id INTEGER PRIMARY KEY,
    player1 bigint(20) ,
    player2 bigint(20) ,
    pokemons_player1 varchar(255),
    pokemons_player2 varchar(255),
    match_status varchar(255) NOT NULL DEFAULT "CREATED",
    winner bigint(20)
)