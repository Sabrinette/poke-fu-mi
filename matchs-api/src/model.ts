
export type MatchStatus = "CREATED" | "IN_PROGRESS" | "FINISHED"

export interface Match {
  //id: number,
  player1: number,
  player2 : number,
  pokemons_player1 : Array<string>,
  pokemons_player2 : Array<string>,
  status : MatchStatus,
  winner : number,
  //rounds : Round[]
}

export interface JwtUser {
  user_id: number,
}