import Database from 'better-sqlite3'
import fs from 'fs'
import { Match, MatchStatus } from './model'

export default class MatchRepository {
  db: Database.Database


  constructor() {
    this.db = new Database('db/pokemon_match.db', { verbose: console.log });
    this.applyMigrations()    
  }

  getAllMatchs(): Match[] {
    const statement = this.db.prepare("SELECT * FROM pokemon_match")
    const rows: Match[] =statement.all()
    return rows
  }

  createMatch(player1: Number,player2: Number,pokemons_player1:Array<string>,pokemons_player2:Array<string>) {
    console.log("%s ", pokemons_player1[0])
    const statement = 
      this.db.prepare("INSERT INTO pokemon_match (player1,player2,pokemons_player1,pokemons_player2) VALUES (?,?,?,?)")
    return statement.run(player1,player2,pokemons_player1,pokemons_player2).lastInsertRowid
  }


  getMatchById(id : number): Match {
    const row = this.db.prepare("SELECT * FROM pokemon_match WHERE pokemon_match.id = ?").get(id)
    return row
  }

  deleteMatch(id : number) {
    const statement = 
      this.db.prepare("DELETE FROM pokemon_match where pokemon_match.id = ?")
    return statement.run(id)
  }

  updateMatch(id : number , player1: Number,player2: Number,pokemons_player1:Array<string>,pokemons_player2:Array<string>,match_status : String, winner :number) {
    const statement = 
      this.db.prepare("UPDATE pokemon_match SET player1 = ? , player2 = ? , pokemons_player1 = ?, pokemons_player2 = ?, match_status = ?, winner = ? where pokemon_match.id = ?")
    return statement.run(player1,player2,pokemons_player1,pokemons_player2,match_status,winner,id)
  }

  applyMigrations(){
    const applyMigration = (path: string) => {
      const migration = fs.readFileSync(path, 'utf8')
      this.db.exec(migration)
    }
    
    const testRow = this.db.prepare("SELECT name FROM sqlite_schema  WHERE type = 'table' AND name = 'pokemon_match'").get()

    if (!testRow){
      console.log('Applying migrations on DB match...')
      const migrations = ['db/migrations/init_match.sql'] 	 
      migrations.forEach(applyMigration)
    }
  }
}

