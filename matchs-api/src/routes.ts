import * as express from "express"
import * as MatchController from "./matchController"
import { Match } from './model'

export const register = ( app: express.Application ) => {
  app.get('/', (req, res) => res.send('Hello World docker!'));

  app.get('/pokemons', async(req, res)  => {
    let userId : number = +req.query.user_id
    await MatchController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })    
    let value = await MatchController.getAllPokemons()
    res.status(200).json(value)
  })

  app.get('/match', async(req, res) => {
    let userId : number = +req.query.user_id
    await MatchController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })
	  res.status(200).json(MatchController.listMatchs())
  })
 
  app.post('/match', async (req, res) => {
    let userId : number = +req.query.user_id
    await MatchController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })
    const newMatch: Match = req.body    
    let result = await MatchController.addMatch(newMatch)
    res.status(200).json(result)
  }) 

  app.get('/match/:matchId', async(req, res) => {
    let userId : number = +req.query.user_id
    await MatchController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })
    if(!MatchController.getMatchById(+req.params.matchId)){
      res.status(404).send()
    }
    res.status(200).json(MatchController.getMatchById(+req.params.matchId))
  })

  app.delete('/match/:matchId', async(req, res) => {
    let userId : number = +req.query.user_id
    if(!MatchController.getMatchById(+req.params.matchId)){
      res.status(404).send()
    }
	  res.status(204).json(MatchController.deleteMatch(+req.params.matchId,userId))
  })  

  app.put('/match/:matchId', async(req, res) => {
    const match: Match = req.body    
    let userId : number = +req.query.user_id
    await MatchController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })  
    res.status(200).json(MatchController.updateMatch(+req.params.matchId,match))
  })

}
