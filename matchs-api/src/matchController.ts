import MatchRepository from './matchRepository'
import { Match } from './model'

const matchRepository = new MatchRepository()

const listMatchs = () => {
    return matchRepository.getAllMatchs()
}
 
const addMatch = (newMatch: Match) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
        if(newMatch.player1 != newMatch.player2){
          playerExist(newMatch.player1).then((result) => {
            if (result)
              playerExist(newMatch.player2).then((res) => {
                if (res)
                  matchRepository.createMatch(newMatch.player1,newMatch.player2,newMatch.pokemons_player1,newMatch.pokemons_player2)
                  resolve(matchRepository.getAllMatchs());
              });
          });
        }
    }, 5000);
  });  
}             


  
const getMatchById = (id: number) => {
  return matchRepository.getMatchById(id)
}

const deleteMatch = (id: number  , idUser : number) => {
  isUserAdmin(idUser).then((result) => {
    if(result)
      console.log("result " ,result)
      matchRepository.deleteMatch(id)
  })
}

const updateMatch = (id: number,match: Match) => {
  matchRepository.updateMatch(id,match.player1,match.player2,match.pokemons_player1,match.pokemons_player2,match.status,match.winner)
  return matchRepository.getMatchById(id)
}

const fetch = require("node-fetch");

async function playerExist(id:number): Promise<boolean> {
  let flag = true
  const response = await fetch("http://players-api:5000/player/" + id)
  const body = await response.text();
  if(!body){
    flag = false
  }
  return flag
}


async function isUserAdmin(id:number): Promise<boolean> {
  let flag = true
  const response = await fetch("http://auth-api:5000/user/role/" + id)
  const body = await response.text();
  if(JSON.parse(body).user_role == "operator"){
    flag = false
  }
  return flag
}

async function isUserConnected(id:number): Promise<boolean> {
  let flag = true
  let value;
  const response = await fetch("http://auth-api:5000/user/accountState/" + id)
  const body = await response.text();
  try{
    value = JSON.parse(body).is_connected
    if(value == 0)
      flag = false //l'utilisateur est déconnecté
  } catch (err){
    flag = false // l'utilisateur n'existe pas
  }
  return flag
}

async function getAllPokemons(): Promise<any> {
  const response = await fetch("https://pokeapi.co/api/v2/pokemon/")
  const body = await response.text();
  return JSON.parse(body)
}

export { listMatchs, addMatch , getMatchById ,deleteMatch,updateMatch,isUserConnected,getAllPokemons}
