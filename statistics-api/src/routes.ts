import * as express from "express"
import * as StatisticsController from "./statisticsController"

export const register = ( app: express.Application ) => {
  app.get('/', (req, res) => res.send('Hello World!'));

  app.get('/count/matchs', async(req, res) => {
    let userId : number = +req.query.user_id
    StatisticsController.isUserConnected(userId).then((result) => {
      if(!result)
          res.status(401).send()
      })
    let value = await StatisticsController.countMatchs()
    res.status(200).json(value);
  })
}