const countMatchs = () => { 
  return new Promise((resolve, reject) => {
           setTimeout(() => {
               getMatchs().then((result) => {
                 resolve(JSON.parse(result).length);
               });
           }, 5000);
 });
}


const fetch = require("node-fetch");

async function getMatchs(): Promise<string> {
  const response = await fetch("http://matchs-api:5000/match/")
  const body = await response.text();
  return body
}

async function isUserConnected(id:number): Promise<boolean> {
  let flag = true
  let value
  const response = await fetch("http://auth-api:5000/user/accountState/" + id)
  const body = await response.text();
  try{
    value = JSON.parse(body).is_connected
    if(value == 0)
      flag = false //l'utilisateur est déconnecté
  }catch (err){
    flag = false // l'utilisateur n'existe pas
  }
  return flag
}

export { countMatchs ,isUserConnected}