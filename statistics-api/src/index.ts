import {app} from './app'
import {AddressInfo} from 'net'// importer un inutilitaire depuis un module net(binder un serveur à mon port)

const server = app.listen(5000, '0.0.0.0', () => { // le port et l'adresse qui vont définir le localhost
    const {port, address} = server.address() as AddressInfo;
    console.log('Server listening on:','http://' + address + ':'+port);
});